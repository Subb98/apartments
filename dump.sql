--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.11
-- Dumped by pg_dump version 9.5.11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: apartments; Type: TABLE; Schema: public; Owner: books
--

CREATE TABLE apartments (
    id integer NOT NULL,
    city character varying(255) NOT NULL,
    street character varying(255) NOT NULL,
    building_num character varying(32) NOT NULL,
    floor_num integer NOT NULL,
    floors_count integer NOT NULL,
    rooms_count integer NOT NULL,
    total_area real NOT NULL,
    price numeric,
    fts text
);


ALTER TABLE apartments OWNER TO books;

--
-- Name: apartments_id_seq; Type: SEQUENCE; Schema: public; Owner: books
--

CREATE SEQUENCE apartments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE apartments_id_seq OWNER TO books;

--
-- Name: apartments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: books
--

ALTER SEQUENCE apartments_id_seq OWNED BY apartments.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: books
--

ALTER TABLE ONLY apartments ALTER COLUMN id SET DEFAULT nextval('apartments_id_seq'::regclass);


--
-- Data for Name: apartments; Type: TABLE DATA; Schema: public; Owner: books
--

COPY apartments (id, city, street, building_num, floor_num, floors_count, rooms_count, total_area, price, fts) FROM stdin;
1	Пермь	Пермская	161	3	9	2	58	3999000.0	'161':3C 'перм':1A 'пермск':2B
2	Пермь	Петропавловская	86	3	5	2	45	2400000.0	'86':3C 'перм':1A 'петропавловск':2B
3	Пермь	25-го Октября	22а	5	5	1	43.7999992	2550000.0	'22а':5C '25':2B 'го':3B 'октябр':4B 'перм':1A
4	Пермь	Тополевый пер.	5	7	10	1	40.7000008	3907200.0	'5':4C 'пер':3B 'перм':1A 'тополев':2B
5	Пермь	Тополевый переулок	5	5	8	1	44.5	4137000.0	'5':4C 'переулок':3B 'перм':1A 'тополев':2B
7	Пермь	Борчанинова	50	2	6	1	45.7000008	3900000.0	'50':3C 'борчанинов':2B 'перм':1A
8	Пермь	Борчанинова	50	3	6	1	38	3570000.0	'50':3C 'борчанинов':2B 'перм':1A
9	Пермь	Петропавловская	86	3	5	2	45	2400000.0	'86':3C 'перм':1A 'петропавловск':2B
10	Казань	Петропавловская	86	3	5	2	45	2400000.0	'86':3C 'казан':1A 'петропавловск':2B
11	Пермь	Борчанинова	50	2	6	1	52.4000015	4820000.0	'50':3C 'борчанинов':2B 'перм':1A
6	Пермь	25-го Октября	22 Б	5	5	1	35	1960000.0	'22':5C '25':2B 'б':6C 'го':3B 'октябр':4B 'перм':1A
\.


--
-- Name: apartments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: books
--

SELECT pg_catalog.setval('apartments_id_seq', 11, true);


--
-- Name: apartments_pkey; Type: CONSTRAINT; Schema: public; Owner: books
--

ALTER TABLE ONLY apartments
    ADD CONSTRAINT apartments_pkey PRIMARY KEY (id);


--
-- Name: building_num_idx; Type: INDEX; Schema: public; Owner: books
--

CREATE INDEX building_num_idx ON apartments USING gin (to_tsvector('russian'::regconfig, (building_num)::text));


--
-- Name: city_idx; Type: INDEX; Schema: public; Owner: books
--

CREATE INDEX city_idx ON apartments USING gin (to_tsvector('russian'::regconfig, (city)::text));


--
-- Name: street_idx; Type: INDEX; Schema: public; Owner: books
--

CREATE INDEX street_idx ON apartments USING gin (to_tsvector('russian'::regconfig, (street)::text));


--
-- PostgreSQL database dump complete
--

