# Apartments

## Prepare to usage
1. Create your own .env file
2. Import dump:
```psql -U <user> <dbname> < dump.sql```
3. Install composer dependencies:
```./bin/composer install```
4. Prepare data:
```chmod +x bin/console && ./bin/console apartments:fts-update```

## Usage example:
```./bin/console apartments:get 8 0```

note: if param 1 is not defined, will take random input row  
note: if param 2 is not defined, floor will be important