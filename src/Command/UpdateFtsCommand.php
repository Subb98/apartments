<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\DB;

class UpdateFtsCommand extends Command
{
    protected function configure()
    {
        // Usage example: ./bin/console apartments:fts-update
        $this->setName('apartments:fts-update')
             ->setDescription('Update fts column in apartments table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbh = DB::getDbh();
        $dbh->query("UPDATE apartments SET fts =
                                setweight(coalesce(to_tsvector(city),''),'A') || ' ' ||
                                setweight(coalesce(to_tsvector(street),''),'B') || ' ' ||
                                setweight(coalesce(to_tsvector(building_num),''),'C')");
    }
}