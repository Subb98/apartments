<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Apartments;

class GetApartmentsCommand extends Command
{
    protected function configure()
    {
        // Usage example: ./bin/console apartments:get 8 0
        $this->setName('apartments:get')
             ->setDescription('Get similar apartment')
             ->addArgument('index', InputArgument::OPTIONAL, 'apartments.id', NULL)
             ->addArgument('strict_floor', InputArgument::OPTIONAL, 'If 1, floor will be important', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $index = $input->getArgument('index');
        $strict_floor = $input->getArgument('strict_floor');

        $apartments = new Apartments($index, $strict_floor);
        $apartments->getSimilarRows();
    }
}