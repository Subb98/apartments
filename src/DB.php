<?php

namespace App;

class DB
{
    private static $dbh;

    private function __construct() {}
    private function __clone () {}

    /**
     * Gets the Database handle instance
     *
     * @return \PDO
     */
    public static function getDbh(): \PDO
    {
        if(!self::$dbh) {
            try {
                self::$dbh = new \PDO("pgsql:host={$_ENV['POSTGRES_HOST']};port={$_ENV['POSTGRES_PORT']};dbname={$_ENV['POSTGRES_DB']}",
                    $_ENV['POSTGRES_USER'], $_ENV['POSTGRES_PASSWORD']);
                self::$dbh->exec('SET NAMES utf8');
                self::$dbh->exec('SET CHARACTER SET utf8');
            } catch(\PDOException $e) {
                echo $e->getMessage();
            }
        }

        return self::$dbh;
    }
}