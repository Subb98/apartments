<?php

namespace App;

class Apartments
{
    private $index;
    private $strict_floor;
    private $dbh;

    public function __construct(int $index = null, bool $strict_floor = true)
    {
        $this->index = $index ?: null;
        $this->strict_floor = $strict_floor;
        $this->dbh = DB::getDbh();
    }

    /**
     * Gets similar rows
     *
     * @return void
     */
    public function getSimilarRows(): void
    {
        if ($this->index) {
            $sth = $this->dbh->prepare('SELECT * FROM apartments WHERE id = :id');
            $sth->execute(['id' => $this->index]);
        } else {
            $sth = $this->dbh->query('SELECT * FROM apartments ORDER BY random() LIMIT 1');
        }

        $input_row = $sth->fetchObject();

        echo "Input row:\n";
        print_r($input_row);
        echo "\n\n";

        // prepare data
        $street = str_replace(' ', '|', $input_row->street);
        $building = preg_replace('/(?!\d+).*/', '', $input_row->building_num) . ':*';

        // prepare query
        $sql = 'SELECT * FROM apartments WHERE to_tsvector(city) @@ to_tsquery(:city)
                    AND to_tsvector(street) @@ to_tsquery(:street)
                    AND to_tsvector(building_num) @@ to_tsquery(:building)
                    AND floors_count = :floors_count
                    AND rooms_count = :rooms_count
                    AND total_area >= :total_area
                    AND price <> :price';

        if ($this->strict_floor) {
            $sql .= " AND floor_num = {$input_row->floor_num}";
        }

        $sth = $this->dbh->prepare($sql);
        $sth->execute([
            'city' => $input_row->city,
            'street' => $street,
            'building' => $building,
            'floors_count' => $input_row->floors_count,
            'rooms_count' => $input_row->rooms_count,
            'total_area' => $input_row->total_area,
            'price' => $input_row->price
        ]);

        echo "Similar row(s):\n";
        while ($row = $sth->fetchObject()) {
            print_r($row);
        }
        echo "\n\n";
    }
}